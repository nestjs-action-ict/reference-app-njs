import { MiddlewareConsumer, Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import {
  CustomLoggerModule,
  RabbitWrapperModule, RabbitWrapperService,
  RequestContextMiddleware,
  RequestContextModule,
} from '@findomestic/msa-commons-njs';

@Module({
  imports: [CustomLoggerModule, RequestContextModule, RabbitWrapperModule],
  controllers: [AppController],
  providers: [AppService, RabbitWrapperService],
})
export class AppModule {
  configure(consumer: MiddlewareConsumer){
    consumer.apply(RequestContextMiddleware).forRoutes('*');

  }


}

import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { CustomLogger, RabbitWrapperService, RequestContextService } from '@findomestic/msa-commons-njs';

@Controller()
export class AppController {

  private readonly logger: CustomLogger;
  private serviceA: RabbitWrapperService;
  private serviceB: RabbitWrapperService;
  constructor(
    private readonly appService: AppService,
    private readonly requestContextService: RequestContextService,
  ) {
    this.serviceA = new RabbitWrapperService({ queue: 'user-messages-a', virtualHost: 'tutorial-host', user: 'guest',pwd: 'guest', address: 'localhost:5672'})
    this.serviceB = new RabbitWrapperService({ queue: 'user-messages-b', virtualHost: 'tutorial-host', user: 'guest',pwd: 'guest', address: 'localhost:5672'})
    this.logger = new CustomLogger(requestContextService, AppController.name);
  }

  @Get('/a')
  getHelloA(): string {
    this.logger.log('Hello World  from A!')
    this.serviceA.emit({msg:  'Message A Sent!'})
    return this.appService.getHello();
  }

  @Get('/b')
  getHelloB(): string {
    this.logger.log('Hello World from B!')
    this.serviceB.emit({msg:  'Message B Sent!'})
    return this.appService.getHello();
  }
}

import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { configLogger, configSwagger, RabbitWrapperService, RabbitParams } from '@findomestic/msa-commons-njs';

async function bootstrap() {

  const app = await NestFactory.create(AppModule);
  await configLogger(app);

  // todo: Inserire i dati di bbase per lo Swagger
  await configSwagger(app, {title: 'Titolo Swagger', description: 'Descrizione API', version: '1.0', tag: 'tag'});

  // todo: Cambiare la porta
  await app.listen(3000);

  const paramsA = new RabbitParams();
  paramsA.user  = 'guest';
  paramsA.pwd = 'guest';
  paramsA.address = 'localhost:5672';
  paramsA.virtualHost = 'tutorial-host';
  paramsA.queue = 'user-messages-a';

   const serviceA = new RabbitWrapperService(paramsA);
   serviceA.listen((msg)  => { console.log('Message received  from  queue A: ', msg)});

  const paramsB = new RabbitParams();
  paramsB.user  = 'guest';
  paramsB.pwd = 'guest';
  paramsB.address = 'localhost:5672';
  paramsB.virtualHost = 'tutorial-host';
  paramsB.queue = 'user-messages-b';

  const serviceB = new RabbitWrapperService(paramsB);
  serviceB.listen((msg)  => { console.log('Message received  from  queue B: ', msg)});


}
bootstrap();
